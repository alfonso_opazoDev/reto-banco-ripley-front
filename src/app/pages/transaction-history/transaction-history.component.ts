import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {TransactionService} from "../../services/rest/transaction.service";
import {TransactionDetailModel} from "../../models/transaction-detail.model";

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css']
})
export class TransactionHistoryComponent implements OnInit {
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['name', 'rut', 'bank', 'account', 'value'];
  public title = "HISTORIAL DE TRANSACCIONES"
  // @ts-ignore
  dataSource: MatTableDataSource<TransactionDetailModel>;

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<TransactionDetailModel>();
    this.getAllTransactions();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getAllTransactions() {
    this.transactionService.getAllTransactions().subscribe((res: TransactionDetailModel[]) => {
      this.dataSource = new MatTableDataSource<TransactionDetailModel>(res)
    })
  }


}
