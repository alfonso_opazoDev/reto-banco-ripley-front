import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/rest/user.service";
import {ClientModel} from "../../models/destination-client.model";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TransactionModel} from "../../models/transaction.model";
import {TransactionService} from "../../services/rest/transaction.service";

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.css']
})
export class TransactionFormComponent implements OnInit {

  public title: string = 'NUEVA TRANSFERNCIA'
  // @ts-ignore
  public selectClient: ClientModel;
  // @ts-ignore
  public transactionForm: FormGroup;
  public listOfResult = [{}];
  public hiddenResultOfSearch: boolean = true;
  RUT = '18.220.258-4'
  findAddresseeValue: string = '';
  // @ts-ignore
  clientSelected: ClientModel = {};
  // @ts-ignore
  transferData: TransactionModel = {};

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private snackBar: MatSnackBar,
              private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.intiForm();
  }

  intiForm() {
    this.transactionForm = this.fb.group({
      findAdrressee: [null, [Validators.required]],
      valueToTransfer: [null, [Validators.required, Validators.min(1), Validators.max(1000000000)]],
    })
  }

  detectKey(event: string) {
    this.findAddresseeValue = event;
    this.userService.findAccountByRutOrName(this.findAddresseeValue).subscribe((res) => {
      this.hiddenResultOfSearch = !(this.findAddresseeValue || res.length === 0);
      this.listOfResult = res;
    })
  }

  getClientDataOnClick(event: any){
    this.clientSelected = event;
    this.listOfResult = [];
  }

  snackBarFunction(text: string, style: string){
    this.snackBar.open(text, "", {
      duration: 1500,
      horizontalPosition: "right",
      verticalPosition: "top",
      panelClass: [style]
    });
  }

  onSubmit(){
    if (this.transactionForm.valid) {
      this.transactionForm.controls['findAdrressee'].setValue(this.clientSelected);
      // @ts-ignore
      const auxTransferData: TransactionModel = {
        originClientRut: this.clientSelected.rut,
        value: this.transactionForm.get('valueToTransfer')?.value,
        DestinationObject: this.transactionForm.get('findAdrressee')?.value,
      }
      this.transactionService.transactionTo(auxTransferData, this.RUT).subscribe();
      this.snackBarFunction('Transferencia exitosa', 'success-style');
      this.transactionForm.reset();
    } else {
      this.snackBarFunction('La transaccion no pudo ser realizada', 'warning-style');
    }
  }


}
