import {ClientModel} from "./destination-client.model";

export interface TransactionModel {
  originClientRut: string
  value: number,
  createdAt: Date,
  DestinationObject: ClientModel
}
